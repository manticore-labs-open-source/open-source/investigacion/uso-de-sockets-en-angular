# Uso de sockets en Angular

## Prerequisitos

* [Levantar un proyecto en Nestjs](https://gitlab.com/manticore-labs/open-source/investigacion/creacion-proyecto-angular)
* [Websockets en Nestjs](https://gitlab.com/manticore-labs/open-source/investigacion/websockets)
* [Creación de un proyecto en Angular](https://gitlab.com/manticore-labs/open-source/investigacion/creacion-proyecto-angular)

La presente investigación trata de complementar lo ya expuesto por los proyectos antes mencionados utilizando sockets desde frontend(Angular) hacia backend(Nesjs).

## Implementación

Hacer uso de sockets desde Angular es bastante sencillo, el primer paso es ubicarse en el componente en el cual se desea hacer uso del socket, luego, se deben seguir los siguientes pasos:

* Importar *_io_* que se encuentra en la siguiente librería:
  
  ``` typescript
  import io from 'socket.io-client';
  ```

* Crear una variable, haciendo llamado al socket:
  
  ``` typescript
  socketBackend = io('http://localhost:3001');
  ```

  **Nota:** Para el ejemplo, se asuma que el socket de Nestjs está levantado en el puerto 3001

* Dentro de la clase del componente, crear un constructor para realizar la conexión con el socket de Nestj así:
  
  ``` typescript
  constructor() {
    this.socketBackend.on('connect', () => {
        console.log('Cliente conectado', this.socketBackend.id);
    })
  }
  ```

* Finalmente, se debe crear un método para llamar al **@SubscribeMessage()** de Nestjs, por ejemplo:
  
  ``` typescript
  saludarAlBackend() {
    this.socketBackend.emit('peticion', '', (respuesta) => {
      console.log('Esperando respuesta del servidor');

      return respuesta;
    });
  }
  ```

Como se puede observar, el uso de sockets en Angular desde Nestjs es bastante sencillo y al no tener que instalar nada extra, reduce la carga a diferencia de Nestjs. En el caso de utilizar servicios a través de sockets, se podría enviar información desde una base de datos desde el socket del backend de tal manera que en el frontend sólo consuma.

<a href="https://twitter.com/following" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @BAndresTorres </a><br>
<a href="https://www.facebook.com/bryan.a.torres.5" target="_blank"><img alt="Sígueme en Facebook" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Facebook"/> Andrés Torres Albuja </a><br>
<a href="https://www.instagram.com/adler.luft/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> adler.luft </a><br>
 